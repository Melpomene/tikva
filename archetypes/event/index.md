---
title: "{{ replace .Name "-" " " | title }}"
subtitle: ""
date: {{ .Date }}
place: CityKirche Konkordien
address: "Mannheim, R 2"
map: "https://goo.gl/maps/uPuawJN5vyN3E3ra7"
performer: Konkordien-Kantorei Mannheim
content: mit Chorwerken *a-cappella*
director: Heike Koefner-Jesatko
draft: true
---

