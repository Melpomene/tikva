---
title: Georg Friedrich Händel — Israel in Egypt (HW 54)
tags: [ "Konzert" ]
eventdate: 2019-12-01T17:00:00+02:00
date: 2019-10-02T00:00:00+02:00
director: Heike Kiefner-Jesatko
place: CityKirche Konkordien
address: "Mannheim, R 2"
map: "https://goo.gl/maps/uPuawJN5vyN3E3ra7"
poster: 2019_12_Israel_in_Egypt_A1_RZ
draft: false
---

<!--more-->

mit {{< sabine_goetz >}} &amp; {{< eva_simonis >}},  
{{< kaspar_kroener >}}, {{< florian_cramer >}},  
{{< joachim_goltz >}} &amp; {{< timothy_sharp >}}

{{< la_banda >}}, {{% kkm %}}
**Leitung**: {{< heike >}}

*Werkeinführung* von [*Ann-Kathrin
Knittel*](https://www.uni-heidelberg.de/fakultaeten/theologie/personen/knittel.html):
ab 12. November [hier](https://qrco.de/bbGzDJ) abrufbar!


Karten nummeriert zu 25,– &euro;, unnummeriert zu 17,– &euro; (14,–
&euro;) und 12,– &euro; (9,– &euro;) an der Abendkasse ab 16 Uhr, an
den Vorverkaufsstellen oder ab Ende Oktober im Pfarrbüro

Unterstützt durch: Regierungspräsidium Karlsruhe, Kulturamt Stadt
Mannheim, LBBW Stiftung Landesbank Baden-Württemberg, Stiftung
Christuskirche – Kirche Christi
