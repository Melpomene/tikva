---
type: events
layout: service
title: "Gottesdienst zum 1. Advent"
subtitle: ""
eventdate: 2020-11-29T11:00:00+01:00
date: 2020-10-01T16:47:30+02:00
place: CityKirche Konkordien
address: "Mannheim, R 2"
map: "https://goo.gl/maps/uPuawJN5vyN3E3ra7"
performer: Konkordien-Kantorei Mannheim
content: mit adventlicher Chormusik *a-cappella*
director: Heike Kiefner-Jesatko
draft: false
---
