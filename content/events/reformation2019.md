---
type: events
layout: service
title: "Reformationsgottesdienst 2019"
subtitle: ""
eventdate: 2019-10-31T19:00:00+01:00
date: 2019-10-01T00:00:00+02:00
place: CityKirche Konkordien
address: "Mannheim, R 2"
map: "https://goo.gl/maps/uPuawJN5vyN3E3ra7"
performer: Konkordien-Kantorei Mannheim
director: Heike Kiefner-Jesatko
draft: false
---

mit Werken von wem auch immer, {{% eva_simonis %}} 
