---
layout: service
title: "Reformationsgottesdienst 2020"
subtitle: ""
eventdate: 2020-10-31T19:00:00Z
date: 2020-10-01T16:47:12+02:00
place: CityKirche Konkordien
address: "Mannheim, R 2"
performer: Konkordien-Kantorei Mannheim
director: Heike Kiefner-Jesatko
draft: false
---

mit Chorwerken *a-cappella*, Angela Keller (Sopran)

