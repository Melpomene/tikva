+++
title = 'Freundeskreis'
weight = 20
+++

Werden Sie **Mitglied im [Freundeskreis der Konkordien-Kantorei
Mannheim]({{< ref "/general/support" >}})** und unterstützen Sie unsere Arbeit.
