+++
title = 'Über uns'
weight = 10
+++

Die [Konkordien-Kantorei Mannheim]({{< ref "/general/choir" >}})
besteht aus etwa 70 Sängerinnen und Sängern aus dem gesamten
Rhein-Neckar-Raum. Wir singen geistliche und weltliche Chormusik von
der Renaissance bis zu Uraufführungen des 21. Jahrhunderts.
