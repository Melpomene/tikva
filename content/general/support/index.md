+++
title = "Fördern"
date = "2019-09-22T12:01:29+02:00"
banner = "foerdern.jpg"
caption = "Die Konkordien-Kantorei zu Besuch bei der deutschen Gemeinde in Brüssel"
weight = 40
draft = false
+++

Als Mitglied des Freundeskreises werden Sie vorab über unsere
Aktivitäten informiert. Gerne reservieren wir Ihnen immer einen festen
Wunschplatz für unsere Konzerte. Werden Sie **Mitglied im Freundeskreis
der Konkordien-Kantorei Mannheim** und unterstützen Sie unsere
Arbeit!

<!--more-->

[Information und Antragsformular](Flyer_Freundeskreis.pdf)
