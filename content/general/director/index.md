+++
title = "Chorleiterin"
date = "2020-04-14T19:58:08+02:00"
weight = 20
banner = "chorleiterin.jpg"
caption = "Chorleiterin Heike Kiefner-Jesatko im Portrait"
draft = false
+++

**Heike Kiefner-Jesatko** studierte an der
Johannes-Gutenberg-Universität Mainz Schulmusik und
Geschichte. Während ihrer Mainzer Studienzeit war sie Assistentin des
Mainzer Bachchores (Prof. Ralf Otto). 
<!--more-->

Nach dem Schulmusikabschluss begann sie ein Dirigierstudium mit
Schwerpunkt Chorleitung an der Musikhochschule Frankfurt
(Prof. Wolfgang Schäfer). Private Klavierstudien bei Prof. Irina
Edelstein sowie diverse Chorleitungs-Meisterkurse (z. B. bei Helmut Rilling, Eric
Ericson u. a.)  ergänzten ihre Ausbildung.

1996 wurde sie als Solorepetitorin mit Dirigierverpflichtung an die
Städtischen Bühnen Osnabrück engagiert.

Von 1998–2001 war sie Chordirektorin des Theaters der Stadt Heidelberg.

Seit dem WS 2001/2002 ist sie [Akademische
Oberrätin](https://www.ph-heidelberg.de/musik/personen/lehrende/kiefner-jesatko-heike.html)
an der Päda&shy;go&shy;gischen Hoch&shy;schule Heidelberg,
unterrichtet dort Musik&shy;theorie, Chorleitung
und leitet den Hochschulchor und den [Frauenchor
4x4](https://www.ph-heidelberg.de/4-x-4-frauenchor/startseite/aktuelles.html)
der [Pädagogischen Hoch&shy;schule
Heidelberg](https://www.ph-heidelberg.de/). 

Mit dem Frauenchor 4x4 wurde sie mit zahlreichen internationalen und
nationalen Preisen ausgezeichnet (u. a. Schumann-Chorpreis
Zwickau&nbsp;2010, Deutscher Chorwettbewerb Weimar 2014, *Grand Prix
of Nations* Göteborg 2019).

Sie ist als Jurorin und Kursleiterin im In- und Ausland tätig.

Im November 2006 erhielt sie den [Landeslehrpreis Baden-Württem&shy;berg](https://mwk.baden-wuerttemberg.de/de/hochschulen-studium/landeslehrpreis/).

Heike Kiefner-Jesatko ist Stipendiatin der
[Richard-Wagner-Stiftung](https://www.wagnermuseum.de/museum/richard-wagner-stiftung-bayreuth/). 

Seit Juni 2006 ist sie musikalische Leiterin der
Kon&shy;kordien-Kantorei Mann&shy;heim und arbeitet sehr erfolgreich
mit international renommierten Solist*innen und Ensembles zusammen.
