+++
title = "Über uns"
date = "2020-05-05T17:00:00+02:00"
banner = "ueber_uns.jpg"
caption = "die Konkordien-Kantorei vor dem Auftritt"
weight = 10
draft = false
+++

Die Konkordien-Kantorei Mannheim besteht aus etwa 70 Sängerinnen und
Sängern aus dem gesamten Rhein-Neckar-Raum. Wir singen geistliche und
weltliche Chormusik von der Renaissance bis zu Uraufführungen
des 21. Jahrhunderts. Unsere Programme gestalten wir mit großen Werken
(Messen, Passionen, Oratorien) ebenso gerne wie mit anspruchsvoller
A-cappella-Literatur.

Charakteristisch für unsere Kantorei sind die stilistisch breit
gefächerte Programmgestaltung, die hohe A-cappella-Kultur und die
Eingliederung in die integrierende Arbeit der [CityKirche
Konkordien](http://www.citygemeinde-hafen-konkordien.de/). Wir wagen
immer wieder auch spartenübergreifende Konzerterlebnisse, die neue
Blickwinkel und Einsichten anbieten, wie etwa einen gemeinsamen
Auftritt mit der [Orientalischen Musikakademie
Mannheim](https://www.orientalischemusikakademie.de/).

Zwei große Konzerte im Jahr mit den zugehörigen Probenwochenenden,
Konzertreisen (z. B. ins Elsass, an den Bodensee, nach Würzburg oder
Flandern) und die Mitgestaltung von
[Gottesdiensten](http://www.citygemeinde-hafen-konkordien.de/gottesdienste/)
bestimmen unseren musikalischen Kalender.

Seit 2006 leitet uns 
[Heike Kiefner-Jesatko]({{< relref "director" >}}) mit großem Erfolg. 
Glaubt man den Konzertrezensenten, schafft sie es immer wieder, dass
wir nicht nur unbekannte oder wenig bekannte Werke „stimmlich
überzeugend“, „ansprechend“ bis „ergreifend“ aufführen, sondern auch
bekannte Werke als „neue Hörerlebnisse“ präsentieren können.

[Weitere Informationen zur Historie der Konkordien-Kantorei Mannheim]( {{< relref "history" >}})

