+++
title = "Historie"
date = "2020-05-05T17:00:00+02:00"
weight = 60
draft= false

[[resources]]
name = "banner"
src= "historie.jpg"
caption = "Silhouette Mannheims mit dem Turm der Konkordienkirche in der Abenddämmerung"
+++

Die Ursprünge der Konkordien-Kantorei Mannheim liegen bereits im
Jahr 1904. Ein gutes Jahr­­­hundert später übernimmt [**Heike
Kiefner-Jesatko**]({{< ref "director" >}}) die Leitung von
**Heinz-Rüdiger Drengemann**, bewahrt einerseits den festen Platz der
Kantorei im Mannheimer Musikleben und führt sie gleichzeitig weiter zu
neuen Herausforderungen.

<!--more-->

Die Konkordien-Kantorei hat eine lange Tradition. Bereits 1904 gab es
an der Konkordienkirche Mannheim einen ersten Chor, der aus dem von
**Albert Hänlein** 1874 gegründeten über­­­kon­­­fessionel­­­len Chor des Vereins
für klassische Kirchenmusik hervorgegangen war. Zur Aufführung unter
Hänlein und seinem Nachfolger **Alfred Wernicke** kamen alte Meister wie
Heinrich Schütz, Palestrina, aber auch damals neue Werke von
Komponisten wie Brahms, Reger, Bruckner, Wolf und Verdi, dessen
_Pater noster_ in Mannheim zum ersten Mal in deutscher Sprache
erklang.

Nach dem Ersten Weltkrieg wurde **Hermann Eckert** Leiter des
Chores. Damals erwuchs den traditionellen Kirchenchören durch die
Singkreise der Jugendmusikbewegung eine Konkurrenz, die dazu führte,
dass den Kirchenchören der Nachwuchs fehlte. Trotzdem gelang es
Eckerts Nachfolger Bruno Penzien, mit dem von ihm gegründeten
„Kurpfälzischen Singkreis“ 1937 Bachs _Weihnachtsoratorium_ und
ein Jahr später Bachs _Magnificat_ in der Konkordienkirche
aufzuführen.

Die Zeit nach dem Zweiten Weltkrieg wurde geprägt von den beiden
Kantoren **Hans Schmidt-Mannheim** und **Dieter
Kreutz**. Schmidt-Mannheim pflegte vor allem die A-cappella-Musik
alter Meister und nannte deshalb den Chor
„Heinrich-Schütz-Kantorei“. Daneben gab es auch einen Kinderchor und
ein Kammerorchester. Dieter Kreutz, Nachfolger Schmidt-Mannheims ab
1968, führte mit der von ihm so genannten „Konkordien-Kantorei“ Werke
wie das _Gloria_ von Vivaldi oder das _Magnificat_ von
Carl Philipp Emanuel Bach auf.

Seit der Übernahme des Chores durch **Heinz-Rüdiger
Drengemann** 1979 zählt die Kantorei zu den
führenden Chören Mann­heims und nimmt mit ihren Kon­zerten einen
festen Platz im Mann­­­heimer Musik­­­leben ein. In dieser Zeit
brachte die Kon­kordien-Kantorei nahezu alle großen Werke des
Oratorien­­repertoires zur Auf­­führung. Der Chor sang u. a. von
Johann Sebastian Bach: _h-Moll-Messe_,
_Weihnachts­oratorium_; von Georg Friedrich Händel:
_Messias_, _Judas Maccabäus_, _Jephta_,
_Alexander­fest_, _Saul_; von Joseph Haydn:
_Jahres­zeiten_, _Nelson Messe_; von Wolfgang Amadeus
Mozart: _Requiem_, _c-Moll-Messe_; von Felix Mendelssohn
Bartholdy: _Paulus_, _Elias_; von Johannes Brahms:
_Ein Deutsches Requiem_; von Giuseppe Verdi: _Requiem_;
von Anton Bruckner: _f-Moll-Messe_; von Carl Orff: _Carmina
Burana_. Aber auch mit weniger bekannten Werken wie Ignaz
Holz­bauers _Miserere_ oder Michael Haydns _Requiem_ und
anspruchsvollen A-cappella-Programmen konnte der Chor das Publikum
begeistern. Zum Schiller­jahr 2005 brachte die Konkordien-Kantorei das
von Max Bruch vertonte _Lied von der Glocke_ zur
Aufführung.

Die Konkordien-Kantorei folgte 1993 und 1994 Einladungen nach
Kattowitz, Krakau und Auschwitz und kon­zertierte im Jahr 1998 auf
einer Schweden-Tournee u. a. in Stockholm. Im Herbst 2005 brachte der
Chor ein Programm mit A-cappella-Literatur der deutschen Romantik in
Paris zur Aufführung.

Seit 2006 leitet **Heike Kiefner-Jesatko** die Konkordien-Kantorei
Mannheim.
