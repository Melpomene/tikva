+++
title = "Mitsingen"
date = "2020-05-05T17:00:00:00+02:00"
banner = "mitsingen.jpg"
caption = "Impression eine Probe"
weight = 30
draft = false
+++

Die Konkordien-Kantorei Mannheim ist ein Chor, der mit seinen
ca. 70 Sänger*innen zwischen 18 und 80 Jahren sicher eine Besonderheit
darstellt.

Wenn Sie Lust auf anspruchsvolle Chorliteratur haben und über
Chorerfahrung verfügen, dann sind Sie in diesem Chor genau
richtig. Sind Sie ein hoher Sopran oder ein hoher Tenor, dann kommen
Sie doch einfach zu einer Probe vorbei.

Neben unseren regelmäßigen Proben und der Teilnahme an
[Gottesdiensten](http://www.citygemeinde-hafen-konkordien.de/gottesdienste/)
gehen wir sehr gerne auf Chorreisen. Dort sind auch unsere Chorkinder
immer dabei. Jedes Alter ist willkommen und für alle Kinder wird in
den probenintensiven Zeiten gesorgt.

Die wöchentlichen Proben unter der Leitung von Heike Kiefner-Jesatko
finden **donnerstags** von **20–22 Uhr** im Haus der evangelischen Kirche 
(**M 1,1**) in Mannheim statt.

Bei weiteren Fragen können Sie sich gerne melden bei: {{< info >}}
