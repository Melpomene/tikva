+++
title = "Konzertpädagogische Projekte"
date = "2020-05-05T17:00:00+02:00"
banner = "pedagogy.jpg"
caption = "Orgelmanual"
weight = 50
draft = false
+++

Die konzertpädagogischen Projekte der Konkordien-Kantorei Mannheim
geben Kindern und Jugendlichen aus Mannheim und der Region die
Möglichkeit, geistliche Musik kennenzulernen.

Dadurch, dass sie unsere Proben besuchen können, Instrumente
ausprobieren dürfen, Fragen stellen können oder sich einfach von den
Klängen mitnehmen lassen, kommen sie in direkten Kontakt mit der Musik
und den Musiker*innen.

Unser nächstes konzertpädagogisches Projekt im März 2021 bereiten wir
für die Matthäuspassion von Johann Sebastian Bach vor.

Gerne stellen wir Ihnen Informationsmaterial zum Werk zur Verfügung
und reservieren Ihnen Karten für die Aufführung (Kinder bis 14 Jahre
haben freien Eintritt).

Wenn Sie sich für diese Arbeit interessieren und mit Ihrer Klasse
einmal eine Probe besuchen möchten, dann wenden Sie sich an folgende
Adresse: 

{{< info >}}
