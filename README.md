# Die neue Website der Konkordienkantorei-Mannheim

## HUGO

Die alte Website der Konkordienkantorei musste letztendlich wegen
einer Sicherheitslücke abgeschaltet werden. Daneben gab es aber noch
weitere Probleme:

- Das Einstellen neuer Inhalte war aufwändig; es gab keinen
  Template-Mechanismus, der für einheitliches Aussehen gesorgt hätte.
- Komplexere Inhalte erforderten Programmierung in HTML, PHP und SQL.
- Als *content management system* erforderte die alte Website den
  Einsatz von
  [HTTP-Cookies](https://de.wikipedia.org/wiki/HTTP-Cookie) und damit
  eine Auseinandersetzung mit den Bestimmungen der
  [EU-Datenschutz-Grundverordnung](https://www.datenschutz-grundverordnung.eu/).
  
Die neue Website basiert auf [HUGO](https://gohugo.io), einem *static
site generator* mit einem elaborierten Vorlagen-System
([Templates](https://gohugo.io/templates/)), das *einfache Erstellung*
und *einheitleiche Darstellung* neuer Inhalte ermöglicht.

Weitere Vorteile sind 

- **Sicherheit**: Es gibt neben der reinen Seitenanforderung keine
  Kommunikation mit dem Webbrowser der Anwender; damit dann die
  Website selbst nicht gehackt werden (höchstens der Sever, auf dem
  sie liegt).
- **Datenschutz**: Das System ist nicht auf HTTP-Cookies oder Daten der
  Nutzer angewiesen; es erfüllt damit auf triviale Weise die
  Anforderungen der Datenschutzgrundverordnung.
  
### Thema

Das Erscheinungsbild der von HUGO generierten Seiten ist durch
[Themen](https://themes.gohugo.io/) beliebig steuerbar. In mehreren
Sitzungen haben wir uns für
[Tikva](https://themes.gohugo.io/hugo-tikva/) entschieden.

## Inhalte

### Konzertankündigungen und weitere Auftritte

Auf der Einstiegsseite werden Konzerte, Auftritten in Gottesdiensten
und so weiter aufgelistet. Die einzelnen Einträge sind mit den
entsprechenden Hauptseiten für das Ereignis verlinkt.

Die Inhalte und das Format dieser Auflistung sind frei steuerbar; sie
wird von HUGO automatisch erstellt.

### allgemeine Informationen

Daneben gibt es feste Inhalte (z. B. zur Chorleiterin oder zur
Konzertpädagogik), die über das Menu oder die Fußzeile aufgerufen
werden können.


## Archetypen

Neue Inhalte können im
[Markdown](https://www.markdownguide.org/basic-syntax/)-Format
bereitgestellt werden; grundlegende Informationen (mit festem Platz
auf der Seite) wie z. B. Titel, Aufführungsdatum und -ort, Plakate
usw. werden in einem Kopfbereich definiert und einheitlich dargestellt.

### Gottesdienste

Hier ein Beispiel für den Auftritt in einem [Gottesdienst](https://melpomene.gitlab.io/tikva/events/reformation2020/):

```
---
layout: service
title: Reformationsgottesdienst 2020
subtitle: 
eventdate: 2020-10-31T19:00:00Z
date: 2020-10-01T16:47:12+02:00
place: CityKirche Konkordien
address: "Mannheim, R 2"
director: Heike Kiefner-Jesatko
draft: false
---

mit Chorwerken *a-cappella*, Angela Keller (Sopran)
```


### Konzerte

Eine
[Konzertankündigung](https://melpomene.gitlab.io/tikva/events/israel_in_egypt_2019)
enthält in der Regel weitere Inforationen und ein Plakat (poster).

```
---
title: Georg Friedrich Händel — Israel in Egypt (HW 54)
eventdate: 2019-12-01T17:00:00+02:00
date: 2019-10-02T00:00:00+02:00
director: Heike Kiefner-Jesatko
place: CityKirche Konkordien
address: Mannheim, R 2
poster: 2019_12_Israel_in_Egypt_A1_RZ
draft: false
---

mit {{​< sabine_goetz >​}} &amp; {{​< eva_simonis >​}},  
{{​< kaspar_kroener >​}}, {{​< florian_cramer >​}},  
{{​< joachim_goltz >​}} &amp; {{​< timothy_sharp >​}}

{{​< la_banda >​}}, {{​% kkm %​}}
**Leitung**: {{​< heike >​}}

*Werkeinführung* von [*Ann-Kathrin
Knittel*](https://www.uni-heidelberg.de/fakultaeten/theologie/personen/knittel.html):
ab 12. November [hier](https://qrco.de/bbGzDJ) abrufbar!

Karten nummeriert zu 25,– &euro;, unnummeriert zu 17,– &euro; 
(14,– &euro;) und 12,– &euro; (9,– &euro;) an der Abendkasse ab 16 Uhr, 
an den Vorverkaufsstellen oder ab Ende Oktober im Pfarrbüro

Unterstützt durch: Regierungspräsidium Karlsruhe, Kulturamt Stadt
Mannheim, LBBW Stiftung Landesbank Baden-Württemberg, Stiftung
Christuskirche – Kirche Christi
```

### Allgemeine Inhalte

Die unveränderlichen, fest-verlinkten Seiten
(z. B. [Mitsingen](https://melpomene.gitlab.io/tikva/general/participate/))
bestehen in der Regel aus einem Einleitungsbild (*banner*) und einem
Text. (Der Parameter `weight` bestimmt die Reihenfolge der Einträge in
der Menuleiste.)

```
---
title: Mitsingen
date: 2020-05-05T17:00:00:00+02:00
banner: mitsingen.jpg
caption: Impression aus einer Chorprobe
weight: 30
draft: false
---

Die Konkordien-Kantorei Mannheim ist ein Chor, der mit seinen
ca. 70 Sänger*innen zwischen 18 und 80 Jahren sicher eine Besonderheit
darstellt.

Wenn Sie Lust auf anspruchsvolle Chorliteratur haben und über
Chorerfahrung verfügen, dann sind Sie in diesem Chor genau
richtig. Sind Sie ein hoher Sopran oder ein hoher Tenor, dann kommen
Sie doch einfach zu einer Probe vorbei.

Neben unseren regelmäßigen Proben und der Teilnahme an
[Gottesdiensten](http://www.citygemeinde-hafen-konkordien.de/gottesdienste/)
gehen wir sehr gerne auf Chorreisen. Dort sind auch unsere Chorkinder
immer dabei. Jedes Alter ist willkommen und für alle Kinder wird in
den probenintensiven Zeiten gesorgt.

Die wöchentlichen Proben unter der Leitung von Heike Kiefner-Jesatko
finden **donnerstags** von **20–22 Uhr** im Haus der evangelischen Kirche 
(**M 1,1**) in Mannheim statt.

Bei weiteren Fragen können Sie sich gerne melden bei: 
[info@konkordien-kantorei-mannheim.de](mailto:info@konkordien-kantorei-mannheim.de)
```

## Shortcodes

Wiederkehrende Textbausteine werden durch
[*Shortcodes*](https://gohugo.io/content-management/shortcodes/) dargestellt, z. B. erscheint
„{{< la_banda >}}“ einfach durch die Angabe von `{{​< la_banda >​}}` im Text.

Die folgenden Shortcodes sind definiert:

| Code | Resultat |
|:---- |:-------- |
| `eva_simonis` | {{< eva_simonis >}} |
| `florian_cramer` | {{< florian_cramer >}} |
| `heike` | {{< heike >}} |
| `info` | {{< info >}} |
| `joachim_goltz` | {{< joachim_goltz >}} |
| `kaspar_kroener` | {{< kaspar_kroener >}} |
| `sabine_goetz` | {{< sabine_goetz >}} |
| `timothy_sharp` | {{< timothy_sharp >}} |

Sobald Eva ihre eigene Website hat, wird einfach der Shortcode
entsprechend geändert, und ihr Name wird auf jeder Seite automatisch verlinkt.

## E-Mail-Adressen

E-Mail-Adressen tauchen nicht im Klartext im HTML-Code auf, sondern
sie werden erst im Browser richtig dargestellt. Dadurch soll
verhindert werden, dass sie automatisch ausgelesen und zu Spam-Zwecke
missbraucht werden können.

```html
<address>E-Mail: 
  <span style="unicode-bidi:bidi-override; direction: rtl;">ed.ierotnak-neidroknok@ofni</span>
</address>

```

## Sidebar

Es ist móglich, bestimmte Inhalte auf jeder Seite darzustellen, z. B. 
- einen kurzen Abschnitt „Über uns“
- eine kurze Information zum Freundeskreis
